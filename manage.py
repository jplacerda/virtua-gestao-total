#!/usr/bin/env python
import os
COV = None
if os.environ.get('FLASK_COVERAGE'):
    import coverage
    COV = coverage.coverage(branch=True, include='app/*')
    COV.start()

if os.path.exists('.env'):
    print('Importing environment from .env...')
    for line in open('.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]

from flask_script import Manager, Shell
from flask_migrate import Migrate, MigrateCommand
from flask_bootstrap import Bootstrap

from app import create_app, db
# from app.auth.models import User, Role, Permission, Follow
# from app.main.models import Post, Comment
# from app.location.models import Country, State, City, District, Address
# from app.building.models import Condominium, Building, Room, Area
# from app.organization.models import Organization, OrganizationUser, Employee, Customer, Supplier

app = create_app(os.getenv('FLASK_CONFIG') or 'default')
manager = Manager(app)
migrate = Migrate(app, db)
bootstrap = Bootstrap(app)

from flask import Flask
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView

# admin = Admin(app, name='virtua_admin', template_mode='bootstrap3')
# admin.add_view(ModelView(OrganizationUser, db.session, endpoint='admin_organization_user'))


def make_shell_context():
    return dict(
        app=app, 
        db=db, 
        # User=User, 
        # Role=Role,
        # Permission=Permission,
        # Follow=Follow, 
        # Post=Post,
        # Comment=Comment,
        # Country=Country,
        # State=State,
        # City=City,
        # District=District,
        # Address=Address,
        # Condominium=Condominium,
        # Building=Building,
        # Room=Room,
        # Area=Area,
        # Organization=Organization,
        # OrganizationUser=OrganizationUser,
        # Employee=Employee,
        # Customer=Customer,
        # Supplier=Supplier
    )
    
manager.add_command("shell", Shell(make_context=make_shell_context))
manager.add_command('db', MigrateCommand)


@manager.command
def test(coverage=False):
    """Run the unit tests."""
    if coverage and not os.environ.get('FLASK_COVERAGE'):
        import sys
        os.environ['FLASK_COVERAGE'] = '1'
        os.execvp(sys.executable, [sys.executable] + sys.argv)
    import unittest
    tests = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner(verbosity=2).run(tests)
    if COV:
        COV.stop()
        COV.save()
        print('Coverage Summary:')
        COV.report()
        basedir = os.path.abspath(os.path.dirname(__file__))
        covdir = os.path.join(basedir, 'tmp/coverage')
        COV.html_report(directory=covdir)
        print('HTML version: file://%s/index.html' % covdir)
        COV.erase()


@manager.command
def profile(length=25, profile_dir=None):
    """Start the application under the code profiler."""
    from werkzeug.contrib.profiler import ProfilerMiddleware
    app.wsgi_app = ProfilerMiddleware(app.wsgi_app, restrictions=[length],
                                      profile_dir=profile_dir)
    app.run()


@manager.command
def deploy():
    """Run deployment tasks."""
    from flask_migrate import upgrade
    from app.auth.models import Role, User

    # migrate database to latest revision
    upgrade()

    # create user roles
    Role.insert_roles()
    User.generate_fake()

    from app.location.models import Country, State, City, District, Address
    Country.generate_fake()
    State.generate_fake()
    City.generate_fake()
    District.generate_fake()
    Address.generate_fake()

    #from app.main.models import *
    #Post.generate_fake(1)
    # create self-follows for all users
    #User.add_self_follows()


if __name__ == '__main__':
    manager.run()