Virtua Gestão Total
===================

Virtua Sistema de Gestão para micros e pequenas empresas.


Como executar o projeto
------------------------

### Montando o ambiente


#### Unix

Install Python 

    sudo apt-get install build-essential
    sudo apt-get install libpq-dev python-dev

Instalar Pip

    sudo apt-get install python-pip

Instalar Virtual Enviroment https://packaging.python.org/installing/#requirements-for-installing-packages

    sudo apt-get install python3-venv
    sudo apt-get install libsqlite3-dev

Criar enviroment
    
    python3 -m venv ~/env/

Ativar enviroment
    
    source ~/env/bin/activate

Atualizar setuptools
    
    pip install -U pip setuptools

Instalando e usando o Bower

    sudo apt-get install nodejs npm
    sudo npm install -g bower
    sudo ln -s /usr/bin/nodejs /usr/bin/node
    bower install

Setting environmet variables

export FLASKY_ADMIN=admin@virtuasi.com.br
export SECRET_KEY=hard-to-guess-string
export MAIL_USERNAME=<Gmail username>
export MAIL_PASSWORD=<Gmail password>


### Executanto o projeto


Criar e entrar na pasta do projeto.

Se estiver rodando o projeto pela primeira vez, crie um settings_local.py, instale as dependências e sincronize o banco de dados.

    pip install -r requirements.txt

Para atualizar o requirements (não é necessário)

    pip install --upgrade -r requirements.txt


Init database

python manage.py db init
python manage.py db migrate -m "initial migration"
python manage.py db upgrade


#Add roles to database and generate fake data

python manage.py shell

from app.auth.models import *
Role.insert_roles()
User.generate_fake()

from app.location.models import *
Country.generate_fake()
State.generate_fake()
City.generate_fake()
District.generate_fake()
Address.generate_fake()

from app.organization.models import *
Supplier.generate_fake()

from app.finance.models import *
FinancialTransaction.generate_fake()

from app.main.models import *
Post.generate_fake(1)


### Usando o Git

Baixando o projeto com Git

sudo apt-get install git
git clone https://jplacerda@bitbucket.org/jplacerda/virtua-gestao-total.git
git config --global user.name <name>
git config --global user.email <email>

Sobrescrevendo os arquivos locais

git fetch --all
git reset --hard origin/master

Sobrecrevendo os arquivos remotos


Commiting Files

1. Start a new feature

git checkout -b new-feature master

2. Edit some files

git add <file>
git commit -m "Start a feature"

3. Merge in the new-feature branch

git checkout master
git merge new-feature
git branch -d new-feature