from flask_wtf import Form
from wtforms import StringField, SelectField, SubmitField
from wtforms.validators import Required, Length

from .models import Country, State, City, District, Address


class CountryForm(Form):
	name = StringField(validators=[Required(), Length(1, 64)])
	code = StringField(validators=[Required(), Length(2,2)])
	

class StateForm(Form):
	name = StringField(validators=[Required(), Length(1, 64)])
	code = StringField(validators=[Required(), Length(2,3)])
	country = SelectField('Country', coerce=int)

	def __init__(self, *args, **kwargs):
		super(StateForm, self).__init__(*args, **kwargs)
		self.country.choices = [(country.id, country.name) 
			for country in Country.query.order_by(Country.name).all()]


class CityForm(Form):
	name = StringField(validators=[Required(), Length(1, 64)])
	code = StringField(validators=[Required(), Length(2,3)])
	state = SelectField('State', coerce=int)

	def __init__(self, *args, **kwargs):
		super(CityForm, self).__init__(*args, **kwargs)
		self.state.choices = [(state.id, state.name) 
			for state in State.query.order_by(State.name).all()]


class DistrictForm(Form):
	name = StringField(validators=[Required(), Length(1, 64)])
	city = SelectField('City', coerce=int)

	def __init__(self, *args, **kwargs):
		super(DistrictForm, self).__init__(*args, **kwargs)
		self.city.choices = [(city.id, city.name) 
			for city in City.query.order_by(City.name).all()]


class AddressForm(Form):
	postal_code = StringField(validators=[Length(0, 10)])
	street = StringField(validators=[Required(), Length(1, 64)])
	complement = StringField(validators=[Length(0, 64)])
	number = StringField(validators=[Required(), Length(1, 8)])
	district = SelectField('District', coerce=int)
	
	def __init__(self, *args, **kwargs):
		super(AddressForm, self).__init__(*args, **kwargs)
		self.district.choices = [(district.id, district.name) 
			for district in District.query.order_by(District.name).all()]
