import json

from flask import Blueprint, render_template, redirect, url_for, flash, request, current_app, jsonify
from flask_login import login_required

import buscacep

from .. import db
# from . import location
from .models import Country, State, City, District, Address
from .forms import CountryForm, StateForm, CityForm, DistrictForm, AddressForm


location = Blueprint('location', __name__, template_folder='templates', url_prefix='/location')


@location.route('/cep/<regex("[0-9]{5}[-.]?[0-9]{3}"):postal_code>')
@login_required
def address_by_postal_code(postal_code):
    address = buscacep.busca_cep_correios(postal_code)
    return address #jsonify(address[0]) if address else '{}'


# Podem ser usados outros webservices como o http://ceplivre.pc2consultoria.com/index.php?module=cep&cep=%s&formato=xml
# Entretanto deve ser observado o encoding para que ele seja alterado
# Outra referencia util http://allissonazevedo.com/2012/03/22/buscando-cep-diretamente-pelo-site-dos-correios-em-python/
# def addressGet(request, zipcode):
#     # Trata o zipcode removendo caracteres diferentes de numeros.
#     # Assim nao precisamos nos preocupar de como vai vir o cep.
#     zipcode = re.sub('[^\d]+', '', zipcode)
#     url = "http://viavirtual.com.br/webservicecep.php?cep=" + zipcode
#     page = urllib2.urlopen(url)
#     print (page)
#     text = page.read().decode('iso-8859-1').encode('utf8')
#     print (text)
#     splitted = text.split('||')
#     return HttpResponse('{"street":"%s","district":"%s","city":"%s","state":"%s"}' % (splitted[0], splitted[1], splitted[2], splitted[4]))


#COUNTRY
@location.route('/country/')
@login_required
def list_country():
    view_name = 'country'
    model = Country
    page = request.args.get('page', 1, type=int)
    pagination = model.query.order_by(Country.name.asc()).paginate(
        page, per_page=current_app.config['FLASKY_POSTS_PER_PAGE'],
        error_out=False)
    items = pagination.items
    return render_template('list.html', items=items, pagination=pagination, view_name=view_name)


@location.route('/country/add/', methods=['GET', 'POST'])
@login_required
def create_country():
    view_name = 'country'
    form = CountryForm()
    if form.validate_on_submit():
        item = Country(
            name=form.name.data,
            code=form.code.data)
        db.session.add(item)
        db.session.commit()
        flash('Item has been recorded.')
        return redirect(url_for('.edit_'+view_name, id=item.id))
    return render_template('form.html', form=form, view_name=view_name)


@location.route('/country/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_country(id):
    view_name = 'country'
    model = Country
    item = model.query.get_or_404(id)
    form = CountryForm()
    if form.validate_on_submit():
        item.name = form.name.data
        item.code = form.code.data
        db.session.add(item)
        flash('Item has been updated.')
        return redirect(url_for('.edit_'+view_name, id=item.id))
    form.name.data = item.name
    form.code.data = item.code
    return render_template('form.html', form=form, id=id, view_name=view_name)


@location.route('/country/<int:id>/delete', methods=['GET', 'POST'])
@login_required
def delete_country(id):
    view_name = 'country'
    model = Country
    item = model.query.get_or_404(id)
    db.session.delete(item)
    flash('Item has been deleted.')
    return redirect(url_for('.list_'+view_name))


#STATE
@location.route('/state/')
@login_required
def list_state():
    view_name = 'state'
    model = State
    page = request.args.get('page', 1, type=int)
    pagination = model.query.order_by(State.name.asc()).paginate(
        page, per_page=current_app.config['FLASKY_POSTS_PER_PAGE'],
        error_out=False)
    items = pagination.items
    return render_template('list.html', items=items, pagination=pagination, view_name=view_name)


@location.route('/state/add/', methods=['GET', 'POST'])
@login_required
def create_state():
    view_name = 'state'
    form = StateForm()
    if form.validate_on_submit():
        item = State(
            name=form.name.data,
            code=form.code.data,
            country_id=form.country.data)
        db.session.add(item)
        db.session.commit()
        flash('Item has been recorded.')
        return redirect(url_for('.edit_'+view_name, id=item.id))
    return render_template('form.html', form=form, view_name=view_name)


@location.route('/state/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_state(id):
    view_name = 'state'
    model = State
    item = mode.query.get_or_404(id)
    form = StateForm()
    if form.validate_on_submit():
        item.name = form.name.data
        item.code = form.code.data
        item.country = Country.query.get(form.country.data)
        db.session.add(item)
        flash('Item has been updated.')
        return redirect(url_for('.edit_'+view_name, id=item.id))
    form.name.data = item.name
    form.code.data = item.code
    form.country.data = item.country_id
    return render_template('form.html', form=form, id=id, view_name=view_name)


@location.route('/state/<int:id>/delete', methods=['GET', 'POST'])
@login_required
def delete_state(id):
    view_name = 'state'
    model = State
    item = model.query.get_or_404(id)
    db.session.delete(item)
    flash('Item has been deleted.')
    return redirect(url_for('.list_'+view_name))


#CITY
@location.route('/city/')
@login_required
def list_city():
    view_name = 'city'
    model = City
    page = request.args.get('page', 1, type=int)
    pagination = model.query.order_by(City.name.asc()).paginate(
        page, per_page=current_app.config['FLASKY_POSTS_PER_PAGE'],
        error_out=False)
    items = pagination.items
    return render_template('list.html', items=items, pagination=pagination, view_name=view_name)


@location.route('/city/add/', methods=['GET', 'POST'])
@login_required
def create_city():
    view_name = 'city'
    form = CityForm()
    if form.validate_on_submit():
        item = City(
            name=form.name.data,
            code=form.code.data,
            state_id=form.state.data)
        db.session.add(item)
        db.session.commit()
        flash('Item has been recorded.')
        return redirect(url_for('.edit_'+view_name, id=item.id))
    return render_template('form.html', form=form, view_name=view_name)


@location.route('/city/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_city(id):
    view_name = 'city'
    model = City
    item = model.query.get_or_404(id)
    form = CityForm()
    if form.validate_on_submit():
        item.name = form.name.data
        item.code = form.code.data
        item.state = State.query.get(form.state.data)
        db.session.add(item)
        flash('Item has been updated.')
        return redirect(url_for('.edit_'+view_name, id=item.id))
    form.name.data = item.name
    form.code.data = item.code
    form.state.data = item.state_id
    return render_template('form.html', form=form, id=id, view_name=view_name)


@location.route('/city/<int:id>/delete', methods=['GET', 'POST'])
@login_required
def delete_city(id):
    view_name = 'city'
    model = City
    item = model.query.get_or_404(id)
    db.session.delete(item)
    flash('Item has been deleted.')
    return redirect(url_for('.list_'+view_name))


#DISTRICT
@location.route('/district/')
@login_required
def list_district():
    view_name = 'district'
    model = District
    page = request.args.get('page', 1, type=int)
    pagination = model.query.order_by(District.name.asc()).paginate(
        page, per_page=current_app.config['FLASKY_POSTS_PER_PAGE'],
        error_out=False)
    items = pagination.items
    return render_template('list.html', items=items, pagination=pagination, view_name=view_name)


@location.route('/district/add/', methods=['GET', 'POST'])
@login_required
def create_district():
    view_name = 'district'
    form = DistrictForm()
    if form.validate_on_submit():
        item = District(
            name=form.name.data,
            city_id=form.city.data)
        db.session.add(item)
        db.session.commit()
        flash('Item has been recorded.')
        return redirect(url_for('.edit_'+view_name, id=item.id))
    return render_template('form.html', form=form, view_name=view_name)


@location.route('/district/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_district(id):
    view_name = 'district'
    model = District
    item = model.query.get_or_404(id)
    form = DistrictForm()
    if form.validate_on_submit():
        item.name = form.name.data
        item.city = City.query.get(form.city.data)
        db.session.add(item)
        flash('Item has been updated.')
        return redirect(url_for('.edit_'+view_name, id=item.id))
    form.name.data = item.name
    form.city.data = item.city_id
    return render_template('form.html', form=form, id=id, view_name=view_name)


@location.route('/district/<int:id>/delete', methods=['GET', 'POST'])
@login_required
def delete_district(id):
    view_name = 'district'
    model = District
    item = model.query.get_or_404(id)
    db.session.delete(item)
    flash('Item has been deleted.')
    return redirect(url_for('.list_'+view_name))


#ADDRESS
@location.route('/address/')
@login_required
def list_address():
    view_name = 'address'
    model = Address
    page = request.args.get('page', 1, type=int)
    pagination = model.query.paginate(
        page, per_page=current_app.config['FLASKY_POSTS_PER_PAGE'],
        error_out=False)
    items = pagination.items
    return render_template('list.html', items=items, pagination=pagination, view_name=view_name)


@location.route('/address/add/', methods=['GET', 'POST'])
@login_required
def create_address():
    view_name = 'address'
    form = AddressForm()
    if form.validate_on_submit():
        item = Address(
            street=form.street.data,
            complement=form.complement.data,
            number=form.number.data,
            postal_code=form.postal_code.data,
            district_id=form.district.data)
        db.session.add(item)
        db.session.commit()
        flash('Item has been recorded.')
        return redirect(url_for('.edit_'+view_name, id=item.id))
    return render_template('form.html', form=form, view_name=view_name)


@location.route('/address/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_address(id):
    view_name = 'address'
    model = Address
    item = model.query.get_or_404(id)
    form = AddressForm()
    if form.validate_on_submit():
        item.street = form.street.data
        item.complement = form.complement.data
        item.number = form.number.data
        item.postal_code = form.postal_code.data
        item.district = District.query.get(form.district.data)
        db.session.add(item)
        flash('Item has been updated.')
        return redirect(url_for('.edit_'+view_name, id=item.id))
    form.street.data = item.street
    form.complement.data = item.complement
    form.number.data = item.number
    form.postal_code.data = item.postal_code
    form.district.data = item.district_id
    return render_template('form.html', form=form, id=id, view_name=view_name)


@location.route('/address/<int:id>/delete', methods=['GET', 'POST'])
@login_required
def delete_address(id):
    view_name = 'address'
    model = Address
    item = model.query.get_or_404(id)
    db.session.delete(item)
    flash('Item has been deleted.')
    return redirect(url_for('.list_'+view_name))
