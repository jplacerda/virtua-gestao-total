from sqlalchemy.exc import IntegrityError
from random import seed, randint
import forgery_py

from .. import db


class Country(db.Model):
    # __tablename__ = "countries"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    code = db.Column(db.String(2)) # not unique as there are duplicates (IT)
    states = db.relationship("State", backref="country")

    @staticmethod
    def generate_fake(count=1):
        seed()
        for i in range(count):
            name = forgery_py.address.country()
            obj = Country(
                    name=name,
                    code=name[0:2],
                    )
            db.session.add(obj)
            try:
                db.session.commit()
            except IntegrityError:
                db.session.rollback()

    def __repr__(self):
        return '%s'%(self.name or self.code)


class State(db.Model):
    # __tablename__ = "states"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    code = db.Column(db.String(3))
    country_id = db.Column(db.Integer, db.ForeignKey('country.id'))
    cities = db.relationship("City", backref="state")
    __table_args__ = (db.UniqueConstraint('name', 'country_id'),)

    @staticmethod
    def generate_fake(count=2):
        seed()
        for i in range(count):
            name = forgery_py.address.state()
            obj = State(
                    name=name,
                    code=name[0:3],
                    country_id=1,
                    )
            db.session.add(obj)
            try:
                db.session.commit()
            except IntegrityError:
                db.session.rollback()

    def __repr__(self):
        return '%s'%(self.name or self.code)
        

class City(db.Model):
    # __tablename__ = "cities"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    code = db.Column(db.String(3))
    state_id = db.Column(db.Integer, db.ForeignKey('state.id'))
    districts = db.relationship("District", backref="city")
    __table_args__ = (db.UniqueConstraint('name', 'state_id'),)

    @staticmethod
    def generate_fake(count=5):
        seed()
        for i in range(count):
            name = forgery_py.address.city()
            obj = City(
                    name=name,
                    code=name[0:3],
                    state_id=randint(1,2)
                    )
            db.session.add(obj)
            try:
                db.session.commit()
            except IntegrityError:
                db.session.rollback()

    def __repr__(self):
        return '%s'%(self.name or self.code)


class District(db.Model):
    # __tablename__ = "districts"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    city_id = db.Column(db.Integer, db.ForeignKey('city.id'))
    addresses = db.relationship("Address", backref="district")
    __table_args__ = (db.UniqueConstraint('name', 'city_id'),)

    @staticmethod
    def generate_fake(count=15):
        seed()
        for i in range(count):
            obj = District(
                    name=forgery_py.lorem_ipsum.title(2),
                    city_id=randint(1,5)
                    )
            db.session.add(obj)
            try:
                db.session.commit()
            except IntegrityError:
                db.session.rollback()

    def __repr__(self):
        return '%s'%(self.name)


class Address(db.Model):
    # __tablename__ = "addresses"
    id = db.Column(db.Integer, primary_key=True)
    street = db.Column(db.String(64))
    complement = db.Column(db.String(64))
    number = db.Column(db.String(8))
    postal_code = db.Column(db.String(10))
    district_id = db.Column(db.Integer, db.ForeignKey('district.id'))
    employee = db.relationship('Employee', backref='address')
    customer = db.relationship('Customer', backref='address')
    supplier = db.relationship('Supplier', backref='address')

    @staticmethod
    def generate_fake(count=100):
        seed()
        for i in range(count):
            obj = Address(
                    street=forgery_py.address.street_name(),
                    complement=forgery_py.lorem_ipsum.title(3),
                    number=forgery_py.address.street_number(),
                    postal_code=forgery_py.address.zip_code(),
                    district_id=randint(1,15)
                    )
            db.session.add(obj)
            try:
                db.session.commit()
            except IntegrityError:
                db.session.rollback()

    def __repr__(self):
        return '%s %s, %s, %s, %s' % (self.street, self.complement, self.number, self.postal_code, self.district)