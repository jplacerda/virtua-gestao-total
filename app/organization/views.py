from flask import Blueprint, render_template, redirect, url_for, flash, request, current_app, jsonify
from flask_login import login_required

from .. import db
# from . import organization

from .models import Plan, Organization, OrganizationUser, Employee, Customer, Supplier
from .forms import OrganizationForm, EmployeeForm, CustomerForm, SupplierForm


organization = Blueprint('organization', __name__, template_folder='templates', url_prefix='/organization')


#organization
@organization.route('/organization/')
@login_required
def list_organization():
    view_name = 'organization'
    model = Organization
    page = request.args.get('page', 1, type=int)
    pagination = model.query.order_by(Organization.name.asc()).paginate(
        page, per_page=current_app.config['FLASKY_POSTS_PER_PAGE'],
        error_out=False)
    items = pagination.items
    return render_template('list.html', items=items, pagination=pagination, view_name=view_name)
    return render_template('form.html', form=form, id=id, view_name=view_name)


@organization.route('/organization/add/', methods=['GET', 'POST'])
@login_required
def create_organization():
    view_name = 'organization'
    form = OrganizationForm()
    if form.validate_on_submit():
        item = Organization(
            domain=form.domain.data,
            name=form.name.data,
            site=form.site.data,
            email=form.email.data,
            billing_email=form.billing_email.data,
            plan=form.plan.data,
            expire_on=form.expire_on.data)
        db.session.add(item)
        db.session.commit()
        flash('Item has been recorded.')
        return redirect(url_for('.edit_'+view_name, id=item.id))
    return render_template('form.html', form=form, view_name=view_name)


@organization.route('/organization/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_organization(id):
    view_name = 'organization'
    model = Organization
    item = model.query.get_or_404(id)
    form = OrganizationForm()
    if form.validate_on_submit():
        item.domain = form.domain.data
        item.name = form.name.data
        item.site = form.site.data
        item.email = form.email.data
        item.billing_email = form.billing_email.data
        item.plan = form.plan.data
        item.expire_on = form.expire_on.data
        db.session.add(item)
        flash('Item has been updated.')
        return redirect(url_for('.edit_'+view_name, id=item.id))
    form.domain.data = item.domain
    form.name.data = item.name
    form.site.data = item.site
    form.email.data = item.email
    form.billing_email.data = item.billing_email
    form.plan.data = item.plan
    form.expire_on.data = item.expire_on
    return render_template('form.html', form=form, id=id, view_name=view_name)


@organization.route('/organization/<int:id>/delete', methods=['GET', 'POST'])
@login_required
def delete_organization(id):
    view_name = 'organization'
    model = Organization
    item = model.query.get_or_404(id)
    db.session.delete(item)
    flash('Item has been deleted.')
    return redirect(url_for('.list_'+view_name))


#employee
@organization.route('/employee/')
@login_required
def list_employee():
    view_name = 'employee'
    model = Employee
    page = request.args.get('page', 1, type=int)
    pagination = model.query.order_by(Employee.user_id.asc()).paginate(
        page, per_page=current_app.config['FLASKY_POSTS_PER_PAGE'],
        error_out=False)
    items = pagination.items
    return render_template('list.html', items=items, pagination=pagination, view_name=view_name)


@organization.route('/employee/add/', methods=['GET', 'POST'])
@login_required
def create_employee():
    view_name = 'employee'
    form = EmployeeForm()
    if form.validate_on_submit():
        item = Employee(
            user_id=form.user.data,
            social_number=form.social_number.data,
            phone=form.phone.data,
            cellphone=form.cellphone.data,
            birthday=form.birthday.data,
            address_id=form.address.data)
        db.session.add(item)
        db.session.commit()
        flash('Item has been recorded.')
        return redirect(url_for('.edit_'+view_name, id=item.id))
    return render_template('form.html', form=form, view_name=view_name)


@organization.route('/employee/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_employee(id):
    view_name = 'employee'
    model = Employee
    item = model.query.get_or_404(id)
    form = EmployeeForm()
    if form.validate_on_submit():
        item.user_id = form.user.data
        item.social_number = form.social_number.data
        item.phone = form.phone.data
        item.cellphone = form.cellphone.data
        item.birthday = form.birthday.data
        item.address_id = form.address.data
        db.session.add(item)
        flash('Item has been updated.')
        return redirect(url_for('.edit_'+view_name, id=item.id))
    form.user.data = item.user_id
    form.social_number.data = item.social_number
    form.phone.data = item.phone
    form.cellphone.data = item.cellphone
    form.birthday.data = item.birthday
    form.address.data = item.address_id
    return render_template('form.html', form=form, id=id, view_name=view_name)


@organization.route('/employee/<int:id>/delete', methods=['GET', 'POST'])
@login_required
def delete_employee(id):
    view_name = 'employee'
    model = Employee
    item = model.query.get_or_404(id)
    db.session.delete(item)
    flash('Item has been deleted.')
    return redirect(url_for('.list_'+view_name))


#customer
@organization.route('/customer/')
@login_required
def list_customer():
    view_name = 'customer'
    model = Customer
    page = request.args.get('page', 1, type=int)
    pagination = model.query.order_by(Customer.name.asc()).paginate(
        page, per_page=current_app.config['FLASKY_POSTS_PER_PAGE'],
        error_out=False)
    items = pagination.items
    return render_template('list.html', items=items, pagination=pagination, view_name=view_name)


@organization.route('/customer/add/', methods=['GET', 'POST'])
@login_required
def create_customer():
    view_name = 'customer'
    form = CustomerForm()
    if form.validate_on_submit():
        item = Customer(
            name=form.name.data,
            email=form.email.data,
            social_number=form.social_number.data,
            phone=form.phone.data,
            cellphone=form.cellphone.data,
            birthday=form.birthday.data,
            address_id=form.address.data)
        db.session.add(item)
        db.session.commit()
        flash('Item has been recorded.')
        return redirect(url_for('.edit_'+view_name, id=item.id))
    return render_template('form.html', form=form, view_name=view_name)


@organization.route('/customer/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_customer(id):
    view_name = 'customer'
    model = Customer
    item = model.query.get_or_404(id)
    form = CustomerForm()
    if form.validate_on_submit():
        item.name = form.name.data
        item.email = form.email.data
        item.social_number = form.social_number.data
        item.phone = form.phone.data
        item.cellphone = form.cellphone.data
        item.birthday = form.birthday.data
        item.address_id = form.address.data
        db.session.add(item)
        flash('Item has been updated.')
        return redirect(url_for('.edit_'+view_name, id=item.id))
    form.name.data = item.name
    form.email.data = item.email
    form.social_number.data = item.social_number
    form.phone.data = item.phone
    form.cellphone.data = item.cellphone
    form.birthday.data = item.birthday
    form.address.data = item.address_id
    return render_template('form.html', form=form, id=id, view_name=view_name)


@organization.route('/customer/<int:id>/delete', methods=['GET', 'POST'])
@login_required
def delete_customer(id):
    view_name = 'customer'
    model = Customer
    item = model.query.get_or_404(id)
    db.session.delete(item)
    flash('Item has been deleted.')
    return redirect(url_for('.list_'+view_name))


#supplier
@organization.route('/supplier/')
@login_required
def list_supplier():
    view_name = 'supplier'
    model = Supplier
    page = request.args.get('page', 1, type=int)
    pagination = model.query.order_by(Supplier.name.asc()).paginate(
        page, per_page=current_app.config['FLASKY_POSTS_PER_PAGE'],
        error_out=False)
    items = pagination.items
    return render_template('list.html', items=items, pagination=pagination, view_name=view_name)


@organization.route('/supplier/add/', methods=['GET', 'POST'])
@login_required
def create_supplier():
    view_name = 'supplier'
    form = SupplierForm()
    if form.validate_on_submit():
        item = Supplier(
            name=form.name.data,
            email=form.email.data,
            social_number=form.social_number.data,
            phone=form.phone.data,
            cellphone=form.cellphone.data,
            birthday=form.birthday.data,
            address_id=form.address.data)
        db.session.add(item)
        db.session.commit()
        flash('Item has been recorded.')
        return redirect(url_for('.edit_'+view_name, id=item.id))
    return render_template('form.html', form=form, view_name=view_name)


@organization.route('/supplier/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_supplier(id):
    view_name = 'supplier'
    model = Supplier
    item = model.query.get_or_404(id)
    form = SupplierForm()
    if form.validate_on_submit():
        item.name = form.name.data
        item.email = form.email.data
        item.social_number = form.social_number.data
        item.phone = form.phone.data
        item.cellphone = form.cellphone.data
        item.birthday = form.birthday.data
        item.address_id = form.address.data
        db.session.add(item)
        flash('Item has been updated.')
        return redirect(url_for('.edit_'+view_name, id=item.id))
    form.name.data = item.name
    form.email.data = item.email
    form.social_number.data = item.social_number
    form.phone.data = item.phone
    form.cellphone.data = item.cellphone
    form.birthday.data = item.birthday
    form.address.data = item.address_id
    return render_template('form.html', form=form, id=id, view_name=view_name)


@organization.route('/supplier/<int:id>/delete', methods=['GET', 'POST'])
@login_required
def delete_supplier(id):
    view_name = 'supplier'
    model = Supplier
    item = model.query.get_or_404(id)
    db.session.delete(item)
    flash('Item has been deleted.')
    return redirect(url_for('.list_'+view_name))