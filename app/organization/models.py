import datetime

from sqlalchemy import event
from sqlalchemy.exc import IntegrityError

from flask_login import current_user

from ..auth.models import User, Permission, Role
from ..product.models import ProductPurchaseOrder, ProductSalesOrder
from ..finance.models import FinancialTransaction

from .. import db


class Plan:
    TEST = 'TS'
    INDIVIDUAL = 'IN'
    MICRO = 'MC'
    SMALL = 'SM'
    MEDIUM = 'MD'
    LARGE = 'LR'
    ALL = (
        (TEST, 'test'),
        (INDIVIDUAL, 'individual'),
        (MICRO, 'micro'),
        (SMALL, 'small'),
        (MEDIUM, 'medium'),
        (LARGE, 'large'),
    )

class Organization(db.Model):

    def get_expire_on(self):
        return datetime.date.today() + datetime.timedelta(days=30)
    
    # __tablename__ = "organizations"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    domain = db.Column(db.String(32), unique=True, index=True)
    site = db.Column(db.String(128), nullable=True)
    email = db.Column(db.String(64))
    billing_email = db.Column(db.String(64))  #remove
    plan = db.Column(db.Enum(Plan.TEST, 
                            Plan.INDIVIDUAL, 
                            Plan.MICRO, 
                            Plan.SMALL, 
                            Plan.MEDIUM, 
                            Plan.LARGE), 
        default=Plan.TEST)
    expire_on = db.Column(db.DateTime(), default=get_expire_on)
    # owner_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    organizations_users = db.relationship('OrganizationUser', 
                                          backref='organization',
                                          cascade='all, delete-orphan')
        
    def on_trial(self):
        return self.plan == TEST and self.expire_on >= datetime.date.today()

    def __repr__(self):
        return self.name


@event.listens_for(Organization, 'after_insert')
def receive_after_insert(mapper, connection, target):
    organization_user = OrganizationUser(
        user_id=current_user.id,
        organization_id=target.id,
        role_id=Role.query.filter_by(name='Administrator').first().id) 
    db.session.add(organization_user)  



class OrganizationUser(db.Model):
    # __tablename__ = "organizations_users"
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))
    organization_id = db.Column(db.Integer, db.ForeignKey('organization.id'))


class Employee(db.Model):
    # __tablename__ = "employees"
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    social_number = db.Column(db.String(16))
    phone = db.Column(db.String(16), nullable=True)
    cellphone = db.Column(db.String(16), nullable=True)
    birthday = db.Column(db.DateTime(), nullable=True)
    address_id = db.Column(db.Integer, db.ForeignKey('address.id'))
    product_purchase_oders = db.relationship('ProductPurchaseOrder', backref='employee')
    product_sales_oders = db.relationship('ProductSalesOrder', backref='employee')
    financial_transactions = db.relationship('FinancialTransaction', backref='employee')

    def __repr__(self):
        return User.query.get(self.user_id).name


class Customer(db.Model):
    # __tablename__ = "customers"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    email = db.Column(db.String(64), nullable=True, unique=True, index=True)
    social_number = db.Column(db.String(16), nullable=True)
    phone = db.Column(db.String(16), nullable=True)
    cellphone = db.Column(db.String(16), nullable=True)
    birthday = db.Column(db.DateTime(), nullable=True)
    address_id = db.Column(db.Integer, db.ForeignKey('address.id'), nullable=True)
    product_sales_oders = db.relationship('ProductSalesOrder', backref='customer')

    def __repr__(self):
        return self.name

# class Parent(Base):
#     __tablename__ = 'left'
#     id = Column(Integer, primary_key=True)
#     children = relationship("Association", back_populates="parent")

class Supplier(db.Model):
    # __tablename__ = "supplier"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    email = db.Column(db.String(64), unique=True, index=True)
    social_number = db.Column(db.String(16))
    phone = db.Column(db.String(16))
    cellphone = db.Column(db.String(16), nullable=True)
    birthday = db.Column(db.DateTime(), nullable=True)
    address_id = db.Column(db.Integer, db.ForeignKey('address.id'), nullable=True)
    products = db.relationship('ProductSupplier', back_populates='supplier')
    product_purchase_oders = db.relationship('ProductPurchaseOrder', backref='supplier')

    @staticmethod
    def generate_fake():
        obj1 = Supplier(id=1, name="Caca e Pesca", email="jplacerda@gmail.com", social_number='11111111111', phone="88888888")
        db.session.add(obj1)
        db.session.commit()
        obj2 = Supplier(id=2, name="Metalurgica Verdadeiro", email="jplacerda1@gmail.com", social_number='22222222222', phone="99999999")
        db.session.add(obj2)
        db.session.commit()

    def __repr__(self):
        return self.name