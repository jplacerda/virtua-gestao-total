from wtforms import StringField, SelectField, DateField
from wtforms.validators import Required, Length, Email, URL, Optional
from flask_wtf import Form

from ..auth.models import User, Role
from ..location.models import Address

from .models import Plan, Organization, OrganizationUser, Employee, Customer, Supplier


class OrganizationForm(Form):
	name = StringField(validators=[Required(), Length(1, 64)])
	domain = StringField(validators=[Required(), Length(1,32)])
	site = StringField(validators=[URL()])
	email = StringField(validators=[Required(), Email()])
	billing_email = StringField(validators=[Required(), Email()])
	plan = SelectField(choices=Plan.ALL, validators=[Required()])
	expire_on = DateField(validators=[Optional()])


class OrganizationUserForm(Form):
	user = SelectField('User', coerce=int, validators=[Required()])
	role = SelectField('Role', coerce=int, validators=[Required()])
	organization = SelectField('Organization', coerce=int, validators=[Required()])

	def __init__(self, *args, **kwargs):
		super(OrganizationUserForm, self).__init__(*args, **kwargs)
		self.user.choices = [(user.id, user.name) 
			for user in User.query.order_by(User.name).all()]
		self.role.choices = [(role.id, role.name) 
			for role in Role.query.order_by(Role.name).all()]
		self.organization.choices = [(organization.id, organization.name) 
			for organization in Organization.query.order_by(Organization.name).all()]


class EmployeeForm(Form):
	user = SelectField('User', coerce=int, validators=[Required()])
	social_number = StringField(validators=[Required(), Length(1,16)])
	phone = StringField(validators=[Length(0,16)])
	cellphone = StringField(validators=[Length(0,16)])
	birthday = DateField(validators=[Optional()])
	address = SelectField('Address', coerce=int, validators=[Required()])

	def __init__(self, *args, **kwargs):
		super(EmployeeForm, self).__init__(*args, **kwargs)
		self.user.choices = [(user.id, user.name) 
			for user in User.query.order_by(User.name).all()]
		self.address.choices = [(address.id, address) 
			for address in Address.query.all()]


class CustomerForm(Form):
	name = StringField(validators=[Required(), Length(1, 64)])
	email = StringField(validators=[Email(), Optional()])
	social_number = StringField(validators=[Length(0,16)])
	phone = StringField(validators=[Length(0,16)])
	cellphone = StringField(validators=[Length(0,16)])
	birthday = DateField(validators=[Optional()])
	address = SelectField('Address', coerce=int)

	def __init__(self, *args, **kwargs):
		super(CustomerForm, self).__init__(*args, **kwargs)
		self.address.choices = [(address.id, address) 
			for address in Address.query.all()]


class SupplierForm(Form):
	name = StringField(validators=[Required(), Length(1, 64)])
	email = StringField(validators=[Required(), Email()])
	social_number = StringField(validators=[Required(), Length(1,16)])
	phone = StringField(validators=[Required(), Length(0,16)])
	cellphone = StringField(validators=[Length(0,16)])
	birthday = DateField(validators=[Optional()])
	address = SelectField('Address', coerce=int, validators=[Optional()])

	def __init__(self, *args, **kwargs):
		super(SupplierForm, self).__init__(*args, **kwargs)
		self.address.choices = [(address.id, address) 
			for address in Address.query.all()]
