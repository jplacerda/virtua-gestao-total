import datetime

# from flask_login import current_user

# from app.organization.models import Employee, Supplier, Customer

from .. import db

class Status:
    CREATED = 'CR'
    PLACED = 'PL'
    PAID = 'PD'
    RECEIVED = 'RC'
    AVAILABLE = 'AV'
    CANCELED = 'CN'
    ALL = (
        (CREATED, 'Created'),
        (PLACED, 'Placed'),
        (PAID, 'Paid'),
        (RECEIVED, 'Received'),
        (AVAILABLE, 'Available'),
        (CANCELED, 'Canceled'),
    )


class ProductSupplier(db.Model):
    supplier_id = db.Column(db.Integer, db.ForeignKey('supplier.id'), primary_key=True)
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'), primary_key=True)
    description = db.Column(db.String(50))
    product = db.relationship("Product", back_populates="suppliers")
    supplier = db.relationship("Supplier", back_populates="products")


class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    code = db.Column(db.String(16))
    value = db.Column(db.Numeric)
    quantity = db.Column(db.SmallInteger)
    suppliers = db.relationship("ProductSupplier", back_populates="product")
    purchase_items = db.relationship('ProductPurchaseItem', backref='product')
    sales_items = db.relationship('ProductSalesItem', backref='product')
    # supplier_id = db.Column(db.Integer, db.ForeignKey('supplier.id'))
    
    def __repr__(self):
        return self.name


class ProductPurchaseOrder(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    employee_id = db.Column(db.Integer, db.ForeignKey('employee.id'))
    supplier_id = db.Column(db.Integer, db.ForeignKey('supplier.id'))
    freight_rate = db.Column(db.Numeric, nullable=True)
    status = db.Column(db.Enum(Status.CREATED, 
            Status.PLACED, 
            Status.PAID, 
            Status.RECEIVED, 
            Status.AVAILABLE,
            Status.CANCELED), 
        default=Status.CREATED)
    financial_transaction_id = db.Column(db.Integer, db.ForeignKey('financial_transaction.id'))
    date = db.Column(db.DateTime)
    items = db.relationship('ProductPurchaseItem', 
                            backref='order',
                            cascade='all, delete-orphan')
   
    @property
    def total_value(self):
        total_value = 0
        for item in self.items:
            if item.quantity and item.value:
                total_value += (item.quantity * item.value)
        return total_value

    def __repr__(self):
        return '%d %s' % (self.id, self.supplier.name)
        

class ProductPurchaseItem(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    order_id = db.Column(db.Integer, db.ForeignKey('product_purchase_order.id'))
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))
    value = db.Column(db.Numeric)
    quantity = db.Column(db.SmallInteger)
    status = db.Column(db.Enum(Status.CREATED, 
            Status.PLACED, 
            Status.PAID, 
            Status.RECEIVED, 
            Status.AVAILABLE,
            Status.CANCELED), 
        default=Status.CREATED)

    @property
    def total_value(self):
        if self.quantity and self.value:
            return self.quantity * self.value
        else:
            return None

    def __repr__(self):
        return self.product.name


class ProductSalesOrder(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    employee_id = db.Column(db.Integer, db.ForeignKey('employee.id'))
    customer_id = db.Column(db.Integer, db.ForeignKey('customer.id'))
    freight_rate = db.Column(db.Numeric, nullable=True)
    status = db.Column(db.Enum(Status.CREATED, 
            Status.PLACED, 
            Status.PAID, 
            Status.RECEIVED, 
            Status.AVAILABLE,
            Status.CANCELED), 
        default=Status.CREATED)
    financial_transaction_id = db.Column(db.Integer, db.ForeignKey('financial_transaction.id'))
    date = db.Column(db.DateTime)
    items = db.relationship('ProductSalesItem', 
                            backref='order',
                            cascade='all, delete-orphan')


class ProductSalesItem(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    order_id = db.Column(db.Integer, db.ForeignKey('product_sales_order.id'))
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))
    value = db.Column(db.Numeric)
    quantity = db.Column(db.SmallInteger)
    discount = db.Column(db.SmallInteger)
    status = db.Column(db.Enum(Status.CREATED, 
            Status.PLACED, 
            Status.PAID, 
            Status.RECEIVED, 
            Status.AVAILABLE,
            Status.CANCELED), 
        default=Status.CREATED)

    @property
    def total_value(self):
        return self.quantity * self.value