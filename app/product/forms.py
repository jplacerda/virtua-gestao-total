from wtforms import StringField, SelectField, DecimalField, IntegerField, DateField
from wtforms.validators import Required, Length, NumberRange, Optional
from flask_wtf import Form

from ..organization.models import Customer, Supplier

from .models import Status, Product


class ProductForm(Form):
    name = StringField(validators=[Required(), Length(1, 64)])
    code = StringField(validators=[Required(), Length(1, 16)])
    value = DecimalField(validators=[Required()])
    quantity = IntegerField(validators=[NumberRange(min=0)])


class ProductPurchaseOrderForm(Form):
    supplier =  SelectField('Supplier', coerce=int, validators=[Required()])

    def __init__(self, *args, **kwargs):
        super(ProductPurchaseOrderForm, self).__init__(*args, **kwargs)
        self.supplier.choices = [(supplier.id, supplier.name) 
            for supplier in Supplier.query.order_by(Supplier.name).all()]


class ProductPurchaseOrderUpdateForm(Form):
    supplier =  SelectField('Supplier', coerce=int, validators=[Required()])
    freight_rate = DecimalField(validators=[Optional()])

    def __init__(self, *args, **kwargs):
        super(ProductPurchaseOrderUpdateForm, self).__init__(*args, **kwargs)
        self.supplier.choices = [(supplier.id, supplier.name) 
            for supplier in Supplier.query.order_by(Supplier.name).all()]


class ProductPurchaseItemForm(Form):
    product =  SelectField('Product', coerce=int, validators=[Required()])
    value = DecimalField(validators=[Required()])
    quantity = IntegerField(validators=[Required(), NumberRange(min=1)])


    def __init__(self, *args, **kwargs):
        super(ProductPurchaseItemForm, self).__init__(*args, **kwargs)
        self.product.choices = [(product.id, product.name) 
            for product in Product.query.order_by(Product.name).all()]


class ProductSalesOrderForm(Form):
    customer =  SelectField('Customer', coerce=int, validators=[Required()])
    freight_rate = DecimalField()

    def __init__(self, *args, **kwargs):
        super(ProductSalesOrderForm, self).__init__(*args, **kwargs)
        self.customer.choices = [(customer.id, customer.name) 
            for customer in Customer.query.order_by(Customer.name).all()]


class ProductSalesItemForm(Form):
    product =  SelectField('Product', coerce=int, validators=[Required()])
    value = DecimalField(validators=[Required()])
    quantity = IntegerField(validators=[Required(), NumberRange(min=1)])
    discount = IntegerField(validators=[NumberRange(min=0)])

    def __init__(self, *args, **kwargs):
        super(ProductSalesItemForm, self).__init__(*args, **kwargs)
        self.product.choices = [(product.id, product.name) 
            for product in Product.query.order_by(Product.name).all()]