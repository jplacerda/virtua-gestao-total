import datetime

from flask import Blueprint, render_template, redirect, url_for, flash, request, current_app, jsonify
from flask_login import login_required, current_user

from ..views import ListView, CreateView, UpdateView, DeleteView
from .. import db
# from . import product

from .models import Status, Product, ProductPurchaseOrder, ProductPurchaseItem, ProductSalesOrder, ProductSalesItem
from .forms import ProductForm, ProductPurchaseOrderForm, ProductPurchaseOrderUpdateForm, ProductPurchaseItemForm, ProductSalesOrderForm, ProductSalesItemForm
from ..organization.models import Supplier
from ..finance.models import FinancialTransaction

product = Blueprint('product', __name__, template_folder='templates', url_prefix='/product')


#product
product.add_url_rule('/product/', view_func=ListView.as_view('list_product', Product))
product.add_url_rule('/product/add/', view_func=CreateView.as_view('create_product', Product, ProductForm))
product.add_url_rule('/product/<int:id>/', view_func=UpdateView.as_view('edit_product', Product, ProductForm))
product.add_url_rule('/product/<int:id>/delete', view_func=DeleteView.as_view('delete_product', Product))
# product.add_url_rule('/purchase/', view_func=ListView.as_view('list_product_purchase_order', ProductPurchaseOrder))


#product_purchase_order
@product.route('/purchase/')
@login_required
def list_product_purchase_order():
    view_name = 'product_purchase_order'
    model = ProductPurchaseOrder
    page = request.args.get('page', 1, type=int)
    pagination = model.query.paginate(page, per_page=current_app.config['FLASKY_POSTS_PER_PAGE'], error_out=False)
    items = pagination.items
    return render_template('purchase_order/list.html', 
        items=items,
        pagination=pagination, 
        view_name=view_name,
        parent_view_name='product_purchase_item')


@product.route('/purchase/add/', methods=['GET', 'POST'])
@login_required
def create_product_purchase_order():
    view_name = 'product_purchase_order'
    form = ProductPurchaseOrderForm()
    if form.validate_on_submit():
        item = ProductPurchaseOrder(
            employee_id=current_user.id,
            supplier_id=form.supplier.data,
            status=Status.CREATED
        )
        db.session.add(item)
        db.session.commit()
        flash('Item has been recorded.')
        return redirect(url_for('.create_product_purchase_item', parent_id=item.id))
    return render_template('form.html', form=form, view_name=view_name)


@product.route('/purchase/<int:id>/delete', methods=['GET', 'POST'])
@login_required
def delete_product_purchase_order(id):
    view_name = 'product_purchase_order'
    model = ProductPurchaseOrder
    item = model.query.get_or_404(id)
    db.session.delete(item)
    flash('Item has been deleted.')
    return redirect(url_for('.list_'+view_name))

# @product.route('/purchase/add/', methods=['GET', 'POST'])
@product.route('/purchase/<int:parent_id>/', methods=['GET', 'POST'])
@product.route('/purchase/<int:parent_id>/<status>/', methods=['GET'])
@login_required
def create_product_purchase_item(parent_id=None, status=None):
    view_name = 'product_purchase_item'

    parent = ProductPurchaseOrder.query.get(parent_id)
    parentForm = ProductPurchaseOrderForm()
    print (parent)

    """
    Se <status> for igual a:
    1. CREATED = somente neste status o pedido pode ser alterado.
    2. PLACED = envia email ao fornecedor solicitando a cotacao.
    3. PAID = registra uma transacao financeira.
    4. AVAILABLE = adiciona a quantidade comprada do item ao produto e lanca o valor da compra como despesa.
    """
    if status:
        print (status)
        parent.status = status
        if status == Status.AVAILABLE:
            for product in parent.items:
                product.product.quantity += product.quantity

            parent.financial_transaction = FinancialTransaction(
                description = 'Purchase Order: %s' % parent,
                value = parent.total_value,
                employee_id=current_user.id)
        # if status == Status.PAID:

            
        # db.session.add(parent)
        return redirect(url_for('.create_'+view_name, parent_id=parent_id))

    form = ProductPurchaseItemForm()
    if form.validate_on_submit():
        item = ProductPurchaseItem(
            order_id=parent_id,
            product_id=form.product.data,
            quantity=form.quantity.data,
            value=form.value.data,
            status=Status.CREATED)
        db.session.add(item)
        db.session.commit()
        flash('Item has been recorded.')
        return redirect(url_for('.edit_'+view_name, 
            parent_id=item.order.id, 
            id=item.id))

    page = request.args.get('page', 1, type=int)
    pagination = ProductPurchaseItem.query.filter_by(order_id=parent_id).paginate(
        page, per_page=current_app.config['FLASKY_POSTS_PER_PAGE'],
        error_out=False)
    items = pagination.items
    return render_template('purchase_item/form_list.html',
        view_name=view_name,
        form=form,
        parentForm=parentForm,
        items=items,
        pagination=pagination, 
        parent=parent)


@product.route('/purchase/<int:parent_id>/item/<int:id>/', methods=['GET', 'POST'])
@login_required
def edit_product_purchase_item(parent_id, id):
    view_name = 'product_purchase_item'
    form = ProductPurchaseItemForm()
    item = ProductPurchaseItem.query.get_or_404(id)
    if form.validate_on_submit():
        item.product_id = form.product.data
        item.quantity = form.quantity.data
        item.value = form.value.data
        db.session.add(item)
        flash('Item has been updated.')
        return redirect(url_for('.edit_'+view_name, 
            id=item.id,
            parent_id=item.order.id))
    form.product.data = item.product_id
    form.quantity.data = item.quantity
    form.value.data = item.value

    page = request.args.get('page', 1, type=int)
    pagination = ProductPurchaseItem.query.filter_by(order_id=parent_id).paginate(
        page, per_page=current_app.config['FLASKY_POSTS_PER_PAGE'],
        error_out=False)
    items = pagination.items
    return render_template('purchase_item/form_list.html', 
        view_name=view_name,
        form=form,
        items=items,
        pagination=pagination, 
        id=id,
        parent=item.order)


@product.route('/purchase/item/<int:id>/delete', methods=['GET', 'POST'])
@login_required
def delete_product_purchase_item(id):
    view_name = 'product_purchase_item'
    model = ProductPurchaseItem
    item = model.query.get_or_404(id)
    parent_id = item.order.id
    db.session.delete(item)
    flash('Item has been deleted.')
    return redirect(url_for('.create_'+view_name, parent_id=parent_id))



#product_purchase_item
# @product.route('/purchase/<int:parent_id>/item/')
# @login_required
# def list_product_purchase_item(parent_id):
#     view_name = 'product_purchase_item'
#     model = ProductPurchaseItem
#     page = request.args.get('page', 1, type=int)
#     pagination = model.query.filter_by(order_id=parent_id).paginate(
#         page, per_page=current_app.config['FLASKY_POSTS_PER_PAGE'],
#         error_out=False)
#     items = pagination.items
#     return render_template('list.html', 
#         items=items, 
#         pagination=pagination, 
#         view_name=view_name,
#         parent_id=parent_id)


# @product.route('/_add_numbers')
# def add_numbers():
#     status = request.args.get('status')
#     parent_id = request.args.get('parent_id')
#     item = ProductPurchaseOrder.query.get(parent_id)
#     item.status = status
#     db.session.add(item)
#     db.session.commit()
#     return jsonify(result=item.status)

# {% block scripts %}
# {{ super() }}
# <script type=text/javascript>
#   $SCRIPT_ROOT = {{ request.script_root|tojson|safe }};
# </script>
# <script type=text/javascript>
#   function order_status(status) {
#       $.getJSON($SCRIPT_ROOT + '/product/_add_numbers', {
#         parent_id: {{ parent.id }},
#         status: status,
#       }, function(data) {
#         $("#statusDiv").load(location.href+" #statusDiv>*","");
#         $("#formDiv").load(location.href+" #formDiv>*","");
#       });
#       return false;
#   }
# </script>
# {% endblock scripts %}


# @login_required
# def edit_product_purchase_order(id):
#     view_name = 'product_purchase_order'
#     model = ProductPurchaseOrder
#     item = model.query.get_or_404(id)
#     form = ProductPurchaseOrderForm()
#     if form.validate_on_submit():
#         item.supplier_id = form.supplier.data
#         db.session.add(item)
#         flash('Item has been updated.')
#         return redirect(url_for('.edit_'+view_name, id=item.id))
#     form.supplier.data = item.supplier_id
#     return render_template('form.html', 
#         form=form, 
#         id=id, 
#         view_name=view_name)
