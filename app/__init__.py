from flask import Flask
from flask_mail import Mail
from flask_moment import Moment
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_pagedown import PageDown
from config import config
# from flask_bootstrap import Bootstrap


mail = Mail()
moment = Moment()
db = SQLAlchemy()
pagedown = PageDown()
# bootstrap = Bootstrap()


login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'auth.login'

try:
    from wtforms.fields import HiddenField
    from wtforms.widgets import HiddenInput
except ImportError:
    def is_hidden_field_filter(field):
        raise RuntimeError('WTForms is not installed.')
else:
    def is_hidden_field_filter(field):
        if isinstance(field, HiddenField):
            return True
        if isinstance(field.widget, HiddenInput):
            return True
        return False
        

from werkzeug.routing import BaseConverter

class RegexConverter(BaseConverter):
    def __init__(self, url_map, *items):
        super(RegexConverter, self).__init__(url_map)
        self.regex = items[0]


#TODO @jplacerda mover filtros para local adequado
import calendar
def month_name(month_number):
    return calendar.month_name[int(month_number)]

def datetimeformat(value, format='%d/%m/%Y %H:%M:%S'):
    return value.strftime(format)


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    
    # Use the RegexConverter function as a converter
    # method for mapped urls
    app.url_map.converters['regex'] = RegexConverter
    app.jinja_env.globals['bootstrap_is_hidden_field'] =\
            is_hidden_field_filter

    app.jinja_env.filters['datetimeformat'] = datetimeformat
    app.jinja_env.filters['month_name'] = month_name
    
    config[config_name].init_app(app)

    # bootstrap.init_app(app)
    mail.init_app(app)
    moment.init_app(app)
    db.init_app(app)
    login_manager.init_app(app)
    pagedown.init_app(app)

    if not app.debug and not app.testing and not app.config['SSL_DISABLE']:
        from flask_sslify import SSLify
        sslify = SSLify(app)

    from .api_1_0 import api as api_1_0_blueprint
    app.register_blueprint(api_1_0_blueprint, url_prefix='/api/v1.0')

    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint, url_prefix='/auth')

    from .building.views import building as building_blueprint
    app.register_blueprint(building_blueprint)

    from .finance.views import finance as finance_blueprint
    app.register_blueprint(finance_blueprint)

    from .location.views import location as location_blueprint
    app.register_blueprint(location_blueprint)

    from .organization.views import organization as organization_blueprint
    app.register_blueprint(organization_blueprint)

    from .product.views import product as product_blueprint
    app.register_blueprint(product_blueprint)

    return app