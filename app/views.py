from flask.views import View
from flask.views import MethodView

from flask_login import login_required

from flask import render_template, redirect, url_for, flash, request, current_app, jsonify

from . import db


# class OrganizationsView():

#     fields = (
#         'domain',
#         'owner_id',
#         'billing_email'
#     )
    

# register_crud(organization, Organization, OrganizationForm, OrganizationsViewy)
# register_crud(organization, OrganizationUser, OrganizationUserForm)
# register_crud(organization, Employee, EmployeeForm)
# register_crud(organization, Customer, CustomerForm)
# register_crud(organization, Supplier, SupplierForm)

def register_crud(app, model, form, fields=None, pk='id', pk_type='int'):
    view_name = model.__tablename__
    url = '/' + view_name + '/'
    app.add_url_rule(url, view_func=ListView.as_view('list_'+view_name, model, fields))
    app.add_url_rule(url+'add', view_func=CreateView.as_view('create_'+view_name, model, form), methods=['GET', 'POST'])
    app.add_url_rule('%s<%s:%s>' % (url, pk_type, pk), view_func=UpdateView.as_view('edit_'+view_name, model, form), methods=['GET', 'POST'])
    app.add_url_rule('%s<%s:%s>' % (url+'delete/', pk_type, pk), view_func=DeleteView.as_view('delete_'+view_name, model))


def register_api(api, model, form, view=None, view_name=None, url=None, pk='id', pk_type='int'):
    if not view_name:
        view_name = model.__tablename__
    if not url and view_name:        
        url = '/' + view_name + '/'
    view_func = ModelAPI.as_view(view_name, view_name, model, form) 
    api.add_url_rule(url, defaults={pk: None, }, view_func=view_func, methods=['GET',])
    api.add_url_rule(url, view_func=view_func, methods=['POST',])
    api.add_url_rule('%s<%s:%s>' % (url, pk_type, pk), view_func=view_func, methods=['GET', 'PUT', 'DELETE'])


class ListView(View):

    decorators=[login_required]
    model = None

    def __init__(self, model, *args, **kwargs):
        super(ListView, self).__init__(*args, **kwargs)
        self.model = model

    def get_template_name(self):
        return 'list.html'

    def dispatch_request(self):
        page = request.args.get('page', 1, type=int)
        pagination = self.model.query.order_by().paginate(page, 
            per_page=current_app.config['FLASKY_POSTS_PER_PAGE'],
            error_out=False)
        return render_template(self.get_template_name(),
            items=pagination.items, 
            pagination=pagination, 
            view_name=self.model.__tablename__)


# class ListView(View):

#     decorators=[login_required]
#     model = None
#     fields = None

#     def __init__(self, model, fields, *args, **kwargs):
#         super(ListView, self).__init__(*args, **kwargs)
#         self.model = model
#         self.fields = fields

#     def get_fields(self):
#         fields = []
#         # for field in self.model.__table__.columns:
#         #     print (dir(field))
#         #     print (field)
#         if hasattr(self, 'fields') and self.fields:
#             fields = [field for field in self.model.__table__.columns if field.name in self.fields]
#         else:
#             fields = self.model.__table__.columns
#         return fields

#     def get_template_name(self):
#         return 'list.html'

#     def dispatch_request(self):
#         page = request.args.get('page', 1, type=int)
#         pagination = self.model.query.order_by().paginate(page, 
#             per_page=current_app.config['FLASKY_POSTS_PER_PAGE'],
#             error_out=False)
#         # print (dir(pagination.items[0].__mapper__.columns))
#         # for i in pagination.items[0].__mapper__.columns:
#         #     print (i)
#         # print ('===================')
#         # print (dir(pagination.items[0].__table__.columns))
#         # for i in pagination.items[0].__table__.columns:
#         #     print (i)
#         return render_template(self.get_template_name(),
#             fields=self.get_fields(),
#             items=pagination.items, 
#             pagination=pagination, 
#             view_name=self.model.__tablename__
#             )


class CreateView(View):
    
    methods = ['GET', 'POST']
    decorators = [login_required]
    model = None
    form = None

    def __init__(self, model, form, *args, **kwargs):
        super(CreateView, self).__init__(*args, **kwargs)
        self.model = model
        self.form = form()

    def get_template_name(self):
        return 'form.html'

    def dispatch_request(self):
        item = self.model()
        form = self.form
        if form.validate_on_submit():
            for field in form:
                print (field.name)
                if field.name != 'csrf_token':
                    setattr(item, field.name, field.data)
            db.session.add(item)
            db.session.commit()
            flash('Item has been recorded.')
            return redirect(url_for('.edit_'+self.model.__tablename__, id=item.id))

        return render_template(self.get_template_name(), 
            form=form, 
            view_name=self.model.__tablename__)


class UpdateView(View):

    methods = ['GET', 'POST']
    decorators = [login_required]
    model = None
    form = None

    def __init__(self, model, form, *args, **kwargs):
        super(UpdateView, self).__init__(*args, **kwargs)
        self.model = model
        self.form = form()

    def get_template_name(self):
        return 'form.html'

    def dispatch_request(self, id):
        item = self.model.query.get_or_404(id)
        form = self.form
        if form.validate_on_submit():
            for field in form:
                if field.name != 'csrf_token':
                    setattr(item, field.name, field.data)
            db.session.add(item)
            flash('Item has been updated.')
            return redirect(url_for('.edit_'+self.model.__tablename__, id=item.id))

        for field in form:
            if field.name != 'csrf_token':
                form_field = getattr(form, field.name)
                form_field.data = getattr(item, field.name)
        
        return render_template(self.get_template_name(),
            form=form, 
            id=id, 
            view_name=self.model.__tablename__)


class DeleteView(View):

    methods = ['GET', 'POST']
    decorators = [login_required]
    model = None

    def __init__(self, model, *args, **kwargs):
        super(DeleteView, self).__init__(*args, **kwargs)
        self.model = model

    def dispatch_request(self, id):
        item = self.model.query.get_or_404(id)
        db.session.delete(item)
        flash('Item has been deleted.')
        return redirect(url_for('.list_'+self.model.__tablename__))


class ModelAPI(MethodView):

    view_name=None
    model = None
    form = None

    def __init__(self, view_name, model, form, *args, **kwargs):
        super(ModelAPI, self).__init__(*args, **kwargs)
        self.view_name = view_name
        self.model = model
        self.form = form()

    def put(self, id):
        print (".PUT PUT PUT PUT PUT PUT ")
        item = self.model.query.get_or_404(id)
        form = self.form
        if form.validate_on_submit():
            for field in form:
                if field.name != 'csrf_token':
                    setattr(item, field.name, field.data)
            db.session.add(item)
            flash('Item has been updated.')
            return redirect(url_for(self.view_name, id=item.id))

        for field in form:
            if field.name != 'csrf_token':
                form_field = getattr(form, field.name)
                form_field.data = getattr(item, field.name)
            form.domain.data = item.domain
        return render_template('form.html', form=form, id=id, view_name=self.view_name)

    def get(self, id):
        if id is None:
            print (".GET GET GET GET GET GET ")
            page = request.args.get('page', 1, type=int)
            pagination = self.model.query.order_by().paginate(page,
                per_page=current_app.config['FLASKY_POSTS_PER_PAGE'],
                error_out=False)
            return render_template(
                'list.html',
                items=pagination.items, 
                pagination=pagination, 
                view_name=self.view_name)
        else:
            return self.put(id)
         
    def post(self):
        print (".POST POST POST POST POST POST ")
        item = self.model
        form = self.form
        if form.validate_on_submit():
            for field in form:
                if field.name != 'csrf_token':
                    setattr(item, field.name, field.data)
            db.session.add(item)
            db.session.commit()
            flash('Item has been recorded.')
            return redirect(url_for(self.view_name, id=item.id))

        return render_template('form.html', form=form, view_name=self.view_name)

    def delete(self, id):
        print (".DELETE DELETE DELETE DELETE DELETE DELETE")
        item = self.model.query.get_or_404(id)
        db.session.delete(item)
        flash('Item has been deleted.')
        return redirect(url_for(self.view_name))