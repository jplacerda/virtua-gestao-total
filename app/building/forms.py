from flask_wtf import Form
from wtforms import StringField, SelectField, SubmitField
from wtforms.validators import Required, Length

from .models import Condominium, Building, Room, Area
from app.location.models import Address


class CondominiumForm(Form):	
	name = StringField(validators=[Required(), Length(1, 64)])
	address = SelectField('Address', coerce=int)

	def __init__(self, *args, **kwargs):
		super(CondominiumForm, self).__init__(*args, **kwargs)
		self.address.choices = [(address.id, address.street) 
			for address in Address.query.order_by(Address.street).all()]


class BuildingForm(Form):
	name = StringField(validators=[Required(), Length(1, 64)])
	condominium = SelectField('Condominium', coerce=int)

	def __init__(self, *args, **kwargs):
		super(BuildingForm, self).__init__(*args, **kwargs)
		self.condominium.choices = [(condominium.id, condominium.name) 
			for condominium in Condominium.query.order_by(Condominium.name).all()]


class RoomForm(Form):
	name = StringField(validators=[Required(), Length(1, 64)])
	building = SelectField('Building', coerce=int)

	def __init__(self, *args, **kwargs):
		super(RoomForm, self).__init__(*args, **kwargs)
		self.building.choices = [(building.id, building.name) 
			for building in Building.query.order_by(Building.name).all()]

class AreaForm(Form):
	name = StringField(validators=[Required(), Length(1, 64)])
	room = SelectField('Building', coerce=int)
	width = StringField(validators=[Required()])
	height = StringField(validators=[Required()])	