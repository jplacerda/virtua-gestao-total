from sqlalchemy.exc import IntegrityError
from random import seed, randint
import forgery_py

from .. import db


class Condominium(db.Model):
    # __tablename__ = "condominiums"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    address = db.Column(db.Integer, db.ForeignKey('address.id')) 
    # buildings = db.relationship("Building", backref="condominium")
    __table_args__ = (db.UniqueConstraint('name', 'address'),) 

    @staticmethod
    def generate_fake(count=10):
        seed()
        for i in range(count):
            name = forgery_py.lorem_ipsum.title(2)
            obj = Condominium(
                    name=name,
                    address=randint(1,100),
                    )
            db.session.add(obj)
            try:
                db.session.commit()
            except IntegrityError:
                db.session.rollback()

    def __repr__(self):
        return '%s'%(self.name)


class Building(db.Model):
    # __tablename__ = "buildings"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    condominium = db.Column(db.Integer, db.ForeignKey('condominium.id'))    

    @staticmethod
    def generate_fake(count=100):
        seed()
        for i in range(count):
            name = forgery_py.lorem_ipsum.title(1),
            obj = Condominium(
                    name=name,
                    condominium=randint(1,10),
                    )
            db.session.add(obj)
            try:
                db.session.commit()
            except IntegrityError:
                db.session.rollback()

    def __repr__(self):
        return '%s'%(self.name)


class Room(db.Model):
    # __tablename__ = "rooms"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    building = db.Column(db.Integer, db.ForeignKey('building.id'))    

    @staticmethod
    def generate_fake(count=100):
        seed()
        for i in range(count):
            name = forgery_py.lorem_ipsum.title(1),
            obj = Condominium(
                    name=name,
                    building=randint(1,100),
                    )
            db.session.add(obj)
            try:
                db.session.commit()
            except IntegrityError:
                db.session.rollback()

    def __repr__(self):
        return '%s'%(self.name)


class Area(db.Model):
    # __tablename__ = "areas"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    width = db.Column(db.Float)
    height = db.Column(db.Float)
    room = db.Column(db.Integer, db.ForeignKey('room.id'))    

    @staticmethod
    def generate_fake(count=100):
        seed()
        for i in range(count):
            name = forgery_py.lorem_ipsum.title(1),
            obj = Condominium(
                    name=name,
                    width = uniform(1,3),
                    height = uniform(1,3),
                    room=randint(1,100),
                    )
            db.session.add(obj)
            try:
                db.session.commit()
            except IntegrityError:
                db.session.rollback()

    def __repr__(self):
        return '%s'%(self.name)