from flask import Blueprint

building = Blueprint('building', __name__, template_folder='templates')

from . import views