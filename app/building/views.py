import json

from flask import render_template, redirect, url_for, flash, request, current_app
from flask_login import login_required

from .. import db
from . import building
from .models import Condominium, Building, Room, Area
from .forms import CondominiumForm, BuildingForm, RoomForm, AreaForm
from ..location.models import Address

#CONDOMINIUM
@building.route('/condominium/')
@login_required
def list_condominium():
    view_name = 'condominium'
    model = Condominium
    query = Condominium.query
    page = request.args.get('page', 1, type=int)
    pagination = query.order_by(Condominium.name.asc()).paginate(
        page, per_page=current_app.config['FLASKY_POSTS_PER_PAGE'],
        error_out=False)
    items = pagination.items
    return render_template('list.html', items=items, pagination=pagination, view_name=view_name)


@building.route('/condominium/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_condominium(id):
    view_name = 'condominium'
    model = Condominium
    item = model.query.get_or_404(id)
    form = CondominiumForm()
    if form.validate_on_submit():
        item.name = form.name.data
        item.address = Address.query.filter_by(street=form.address.data).first()
        db.session.add(item)
        flash('Item has been updated.')
        return redirect(url_for('.edit_'+view_name, id=item.id))
    form.name.data = item.name
    form.address.data = item.address
    return render_template('form.html', form=form, id=id, view_name=view_name)


@building.route('/condominium/add/', methods=['GET', 'POST'])
@login_required
def create_condominium():
    view_name = 'condominium'
    model = Condominium
    form = CondominiumForm()
    if form.validate_on_submit():
        item = Condominium(
            name=form.name.data,
            address=form.address.data)
        db.session.add(item)
        db.session.commit()
        flash('Item has been recorded.')
        return redirect(url_for('.edit_'+view_name, id=item.id))
    return render_template('form.html', form=form, view_name=view_name)


@building.route('/condominium/delete/<int:id>', methods=['GET', 'POST'])
@login_required
def delete_condominium(id):
    view_name = 'condominium'
    model = Condominium
    item = Condominium.query.get_or_404(id)
    db.session.delete(item)
    flash('Item has been deleted.')
    return redirect(url_for('.list_'+view_name))


#BUILDING
@building.route('/building/')
@login_required
def list_building():
    view_name = 'building'
    model = Building
    query = Building.query
    page = request.args.get('page', 1, type=int)
    pagination = query.order_by(Building.name.asc()).paginate(
        page, per_page=current_app.config['FLASKY_POSTS_PER_PAGE'],
        error_out=False)
    items = pagination.items
    return render_template('list.html', items=items, pagination=pagination, view_name=view_name)


@building.route('/building/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_building(id):
    view_name = 'building'
    model = Building
    item = Building.query.get_or_404(id)
    form = BuildingForm()
    if form.validate_on_submit():
        item.name = form.name.data        
        item.condominium = Condominium.query.get(form.condominium.data)
        db.session.add(item)
        flash('Item has been updated.')
        return redirect(url_for('.edit_'+view_name, id=item.id))
    form.name.data = item.name
    form.condominium.data = item.condominium
    return render_template('form.html', form=form, id=id, view_name=view_name)


@building.route('/building/add/', methods=['GET', 'POST'])
@login_required
def create_building():
    view_name = 'building'
    model = Building
    form = BuildingForm()
    if form.validate_on_submit():
        item = Building(
            name=form.name.data,            
            condominium=form.condominium.data)
        db.session.add(item)
        db.session.commit()
        flash('Item has been recorded.')
        return redirect(url_for('.edit_'+view_name, id=item.id))
    return render_template('form.html', form=form, view_name=view_name)


@building.route('/building/delete/<int:id>', methods=['GET', 'POST'])
@login_required
def delete_building(id):
    view_name = 'building'
    model = Building
    item = Building.query.get_or_404(id)
    db.session.delete(item)
    flash('Item has been deleted.')
    return redirect(url_for('.list_'+view_name))


#ROOM
@building.route('/room/')
@login_required
def list_room():
    query = Room.query
    page = request.args.get('page', 1, type=int)
    pagination = query.order_by(Room.name.asc()).paginate(
        page, per_page=current_app.config['FLASKY_POSTS_PER_PAGE'],
        error_out=False)
    items = pagination.items
    return render_template('room/list.html', items=items, pagination=pagination)


@building.route('/room/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_room(id):
    item = Room.query.get_or_404(id)
    form = RoomForm()
    if form.validate_on_submit():
        item.name = form.name.data
        item.building = Building.query.get(form.building.data)
        db.session.add(item)
        flash('Item has been updated.')
        return redirect(url_for('.edit_room', id=item.id))
    form.name.data = item.name    
    form.building.data = item.building_id
    return render_template('room/form.html', form=form, id=id)


@building.route('/room/add/', methods=['GET', 'POST'])
@login_required
def create_room():
    form = RoomForm()
    if form.validate_on_submit():
        item = Room(
            name=form.name.data,
            building_id=form.building.data)
        db.session.add(item)
        db.session.commit()
        flash('Item has been recorded.')
        return redirect(url_for('.edit_room', id=item.id))
    return render_template('room/form.html', form=form)


@building.route('/room/delete/<int:id>', methods=['GET', 'POST'])
@login_required
def delete_room(id):
    item = Room.query.get_or_404(id)
    db.session.delete(item)
    flash('Item has been deleted.')
    return redirect(url_for('.list_room'))


#AREA
@building.route('/area/')
@login_required
def list_area():
    query = Area.query
    page = request.args.get('page', 1, type=int)
    pagination = query.order_by(Area.name.asc()).paginate(
        page, per_page=current_app.config['FLASKY_POSTS_PER_PAGE'],
        error_out=False)
    items = pagination.items
    return render_template('area/list.html', items=items, pagination=pagination)


@building.route('/area/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_area(id):
    item = Area.query.get_or_404(id)
    form = AreaForm()
    if form.validate_on_submit():
        item.name = form.name.data
        item.room = Room.query.get(form.room.data)
        item.width = form.width.data
        item.height = form.height.data
        db.session.add(item)
        flash('Item has been updated.')
        return redirect(url_for('.edit_area', id=item.id))
    form.name.data = item.name
    form.room.data = item.room_id
    form.width.data = item.width
    form.height.data = item.height
    return render_template('area/form.html', form=form, id=id)


@building.route('/area/add/', methods=['GET', 'POST'])
@login_required
def create_area():
    form = AreaForm()
    if form.validate_on_submit():
        item = Area(
            name=form.name.data,
            room_id=form.room.data,
            width=form.width.data,
            height=form.height.data)
        db.session.add(item)
        db.session.commit()
        flash('Item has been recorded.')
        return redirect(url_for('.edit_area', id=item.id))
    return render_template('area/form.html', form=form)


@building.route('/area/delete/<int:id>', methods=['GET', 'POST'])
@login_required
def delete_area(id):
    item = Area.query.get_or_404(id)
    db.session.delete(item)
    flash('Item has been deleted.')
    return redirect(url_for('.list_area'))