import datetime
from dateutil.relativedelta import relativedelta

from sqlalchemy.sql import func

from flask import Blueprint, render_template, redirect, request, url_for, flash, current_app
from flask_login import current_user, login_required

from .. import db

from ..views import ListView, CreateView, UpdateView, DeleteView

from .models import FinancialTransaction
from .forms import FinancialTransactionForm

finance = Blueprint('finance', __name__, template_folder='templates', url_prefix='/finance')


#finance
finance.add_url_rule('/', view_func=ListView.as_view('list_financial_transaction', FinancialTransaction))
finance.add_url_rule('/add/', view_func=CreateView.as_view('create_financial_transaction', FinancialTransaction, FinancialTransactionForm))
finance.add_url_rule('/<int:id>/', view_func=UpdateView.as_view('edit_financial_transaction', FinancialTransaction, FinancialTransactionForm))
finance.add_url_rule('/<int:id>/delete', view_func=DeleteView.as_view('delete_financial_transaction', FinancialTransaction))

today = datetime.date.today()

@finance.route('/report/daily/', defaults={'year':today.year, 'month':today.month, 'day':today.day})
@finance.route('/report/<int:year>/<int:month>/<int:day>/')
@login_required
def report_by_day(year, month, day):
    view_name = 'report_by_day'
    model = FinancialTransaction
    today = datetime.date.today()
        
    date = datetime.date(year=year, month=month, day=day)
    previous_date = date - relativedelta(days=1)
    next_date = date + relativedelta(days=1)
    
    items = model.query.filter(func.extract('year', model.date)==year, func.extract('month', model.date)==month, func.extract('day', model.date)==day).all()
    items_func = db.session.query(func.sum(model.value).label('total_value')).filter(func.extract('year', model.date)==year, func.extract('month', model.date)==month, func.extract('day', model.date)==day)
    total_value = items_func.first().total_value

    return render_template('report_by_day.html', 
        items=items,
        view_name=view_name,
        year=year,
        month=month,
        day=day,
        today=today,
        previous_date=previous_date,
        next_date=next_date,
        total_value=total_value)


# @finance.route('/report/')
@finance.route('/report/monthly/', defaults={'year':today.year, 'month':today.month})
@finance.route('/report/<int:year>/<int:month>/', methods=['GET', 'POST'])
@login_required
def report_by_month(year, month):
    view_name = 'report_by_month'
    model = FinancialTransaction
    today = datetime.date.today()
    
    year = today.year if not year else year
    month = today.month if not month else month   
    day = today.day if month == today.month else 1
    
    date = datetime.date(year=year, month=month, day=day)
    previous_date = date - relativedelta(months=1)
    next_date = date + relativedelta(months=1)
    
    items = model.query.filter(func.extract('year', model.date)==year, func.extract('month', model.date)==month).all()
    items_func = db.session.query(func.sum(model.value).label('total_value')).filter(func.extract('year', model.date)==year, func.extract('month', model.date)==month)
    total_value = items_func.first().total_value
    
    return render_template('report_by_month.html', 
        items=items,
        view_name=view_name,
        year=year,
        month=month,
        day=day,
        today=today,
        previous_date=previous_date,
        next_date=next_date,
        total_value=total_value)


@finance.route('/report/yearly/', defaults={'year':today.year})
@finance.route('/report/<int:year>/')
@login_required
def report_by_year(year):
    view_name = 'report_by_year'
    model = FinancialTransaction
    today = datetime.date.today()
        
    month = today.month if year == today.year else 1
    day = today.day if month == today.month else 1

    date = datetime.date(year=year, month=month, day=day)
    previous_date = date - relativedelta(years=1)
    next_date = date + relativedelta(years=1)
    
    items = model.query.filter(func.extract('year', model.date)==year).all()
    items_func = db.session.query(func.sum(model.value).label('total_value')).filter(func.extract('year', model.date)==year)
    total_value = items_func.first().total_value
    
    return render_template('report_by_year.html', 
        items=items,
        view_name=view_name,
        year=year,
        month=month,
        day=day,
        today=today,
        previous_date=previous_date,
        next_date=next_date,
        total_value=total_value)

@finance.route('/report/transaction/', defaults={'transaction_id':None})
@finance.route('/report/transaction/<transaction_id>/')
@login_required
def report_by_transaction(transaction_id):
    view_name = 'report_by_transaction'
    model = FinancialTransaction
    
    if transaction_id:
        items = model.query.filter_by(transaction_id=transaction_id).all()
    else:
        items = model.query.all()
    items_func = db.session.query(func.sum(model.value).label('total_value')).filter(model.transaction_id==transaction_id)
    total_value = items_func.first().total_value
 
    return render_template('report_by_transaction.html', 
        items=items,
        view_name=view_name,
        transaction_id=transaction_id,
        total_value=total_value)



# @finance.route('/add/', methods=['GET', 'POST'])
# def create_financial_transaction():
# 	print dir(current_user)
# 	view_name = 'financial_transaction'
# 	form = FinancialTransactionForm()
# 	if form.validate_on_submit():
# 	    item = FinancialTransaction(
# 	        description=form.description.data,
# 	        value=form.value.data,
# 	        employee_id=current_user.id)
# 	    db.session.add(item)
# 	    db.session.commit()
# 	    flash('Item has been recorded.')
# 	    return redirect(url_for('.edit_'+view_name, id=item.id))
# 	return render_template('form.html', form=form, view_name=view_name)