import datetime
import uuid

from sqlalchemy.orm import backref

from ..auth.models import User
# from ..organization.models import Employee
from flask_login import current_user

from .. import db


class Operation:
    CREDIT = 'CR'
    DEBIT = 'DB'
    OPERATION = (
        (CREDIT, 'Credit'),
        (DEBIT, 'Debit'),
    )

class TimeUnit:
    DAY = 'D'
    MONTH = 'M'
    YEAR = 'Y'
    UNIQUE = 'U'
    TIME_UNIT = (
        (UNIQUE, 'unique'),
        (DAY, 'day'),
        (MONTH, 'month'),
        (YEAR, 'year'),
      )


class FinancialTransaction(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(64))
    value = db.Column(db.Numeric)
    date = db.Column(db.DateTime(), default=datetime.datetime.utcnow)
    transaction_id = db.Column(db.String(16), default=str(uuid.uuid4()))
    employee_id = db.Column(db.Integer, db.ForeignKey('employee.id'))
    product_purchase_oders = db.relationship('ProductPurchaseOrder', backref=backref('financial_transaction', cascade='all'))
    product_sales_oders = db.relationship('ProductSalesOrder', backref=backref('financial_transaction', cascade='all'))

    @property
    def total_group_value(self):
        total_value = 0
        items = self.query.filter_by(transaction_id=self.transaction_id)
        for item in items:
            total_value += item.value
        return total_value

    def __repr__(self):
        return self.description

