from wtforms import StringField, SelectField, DecimalField, IntegerField, DateTimeField
from wtforms.validators import Required, Length, NumberRange, Optional
from flask_wtf import Form

from ..organization.models import Customer, Supplier

from .models import FinancialTransaction


class FinancialTransactionForm(Form):
    description = StringField(validators=[Required(), Length(1, 64)])
    value = DecimalField(validators=[Required()])
    date = DateTimeField(validators=[Required()])
    transaction_id = StringField(validators=[Required(), Length(16)])
    employee_id = StringField(validators=[Required()])